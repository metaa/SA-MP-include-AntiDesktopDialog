/*****************************************************************************************************************************
**                                                                                                                          **
**  AntiDesktopDialog v0.2 by Meta                                                                                          **
**                                                                                                                          **
**  It's a simple solution for I-came-from-desktop-with-ESC-or-return-and-answered-the-dialog-problems!                     **
**  This include makes the dialog display again and aborts any processing from the dialog that has been sent by mistake.    **
**                                                                                                                          **
**  Copyright 2011 Meta (metax@freenet.de, http://meta.spitnex.de/?lang=en). All rights reserved.                           **
**                                                                                                                          **
*****************************************************************************************************************************/

// Detect wheather the player is AFK.
public OnPlayerUpdate(playerid)
{
	if(GetPVarType(playerid, "DesktopTimer") == 0)
	{
		SetPVarInt(playerid, "DesktopTimer", SetTimerEx("Desktop_Function", 1000, 1, "d", playerid));
	}
	SetPVarInt(playerid, "DesktopCount", GetPVarInt(playerid, "DesktopCount")+1);
	CallLocalFunction("ADD_OnPlayerUpdate", "d", playerid);
	return 1;
}

#if defined _ALS_OnPlayerUpdate
	#undef OnPlayerUpdate
#else
	#define _ALS_OnPlayerUpdate
#endif
#define OnPlayerUpdate ADD_OnPlayerUpdate

public OnPlayerDisconnect(playerid, reason)
{
	KillTimer(GetPVarInt(playerid, "DesktopTimer"));
	return CallLocalFunction("ADD_OnPlayerDisconnect", "dd", playerid, reason);
}

#if defined _ALS_OnPlayerDisconnect
	#undef OnPlayerDisconnect
#else
	#define _ALS_OnPlayerDisconnect
#endif
#define OnPlayerDisconnect ADD_OnPlayerDisconnect

// Processing a count of seconds the player's been AFK. Needed later.
public Desktop_Function(playerid)
{
	if(GetPlayerState(playerid) != PLAYER_STATE_WASTED && !IsPlayerNPC(playerid) && IsPlayerConnected(playerid))
	{
		if(GetPVarInt(playerid, "DesktopCount") > 0) { SetPVarInt(playerid, "DesktopStatus", 0); }
		else if(GetPVarInt(playerid, "DesktopCount") <= 0) { SetPVarInt(playerid, "DesktopStatus", GetPVarInt(playerid, "DesktopStatus")+1); }
		DeletePVar(playerid, "DesktopCount");
	}
	return 1;
}

// Returns the PVar's String. By Meta.
stock ReturnPString(playerid, varname[])
{
	new varstring[256]; // Make it longer if you've got some dialogs with huge texts. But use it with caution because if this is too big, the dialog probably won't be displayed again!
	GetPVarString(playerid, varname, varstring, sizeof(varstring));
	return varstring;
}

// Catch the dialog that has been sent by mistake and let it pop up again.
public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	if(GetPVarInt(playerid, "DesktopStatus") > 1 && GetPVarType(playerid, "PrevDialogID"))
	{
		return ShowPlayerDialog(playerid, GetPVarInt(playerid, "PrevDialogID"), GetPVarInt(playerid, "PrevDialogStyle"), ReturnPString(playerid, "PrevDialogCaption"), ReturnPString(playerid, "PrevDialogInfo"), ReturnPString(playerid, "PrevDialogButton1"), ReturnPString(playerid, "PrevDialogButton2"));
	}
	DeletePVar(playerid, "PrevDialogID");
	DeletePVar(playerid, "PrevDialogStyle");
	DeletePVar(playerid, "PrevDialogCaption");
	DeletePVar(playerid, "PrevDialogInfo");
	DeletePVar(playerid, "PrevDialogButton1");
	DeletePVar(playerid, "PrevDialogButton2");
	if(inputtext[0] == '\0' || !strlen(inputtext)) { format(inputtext, 128, " "); }
	return CallLocalFunction("ADD_OnDialogResponse", "dddds", playerid, dialogid, response, listitem, inputtext);
}
#if defined _ALS_OnDialogResponse
	#undef OnDialogResponse
#else
	#define _ALS_OnDialogResponse
#endif
#define OnDialogResponse ADD_OnDialogResponse


// Catch the Dialog's Data and store it into PVars.
public ShowPlayerDialogWithAntiDesktop(playerid, dialogid, style, caption[], info[], button1[], button2[])
{
	if(dialogid >= 0)
	{
		SetPVarInt(playerid, "PrevDialogID", dialogid);
		SetPVarInt(playerid, "PrevDialogStyle", style);
		SetPVarString(playerid, "PrevDialogCaption", caption);
		SetPVarString(playerid, "PrevDialogInfo", info);
		SetPVarString(playerid, "PrevDialogButton1", button1);
		SetPVarString(playerid, "PrevDialogButton2", button2);
	}
	else
	{
		DeletePVar(playerid, "PrevDialogID");
		DeletePVar(playerid, "PrevDialogStyle");
		DeletePVar(playerid, "PrevDialogCaption");
		DeletePVar(playerid, "PrevDialogInfo");
		DeletePVar(playerid, "PrevDialogButton1");
		DeletePVar(playerid, "PrevDialogButton2");
	}
	//SendClientMessage(playerid, 0xFFFFFFFF, "ShowPlayerDialog"); // Debug
	return ShowPlayerDialog(playerid, dialogid, style, caption, info, button1, button2);
}

#if defined _ALS_ShowPlayerDialog
	#undef ShowPlayerDialog
#else
	#define _ALS_ShowPlayerDialog
#endif
#define ShowPlayerDialog ShowPlayerDialogWithAntiDesktop

forward ADD_OnPlayerUpdate(playerid);
forward ADD_OnPlayerDisconnect(playerid, reason);
forward Desktop_Function(playerid);
forward ADD_OnDialogResponse(playerid, dialogid, response, listitem, inputtext[]);
forward ShowPlayerDialogWithAntiDesktop(playerid, dialogid, style, caption[], info[], button1[], button2[]);

/* == EOF == */
