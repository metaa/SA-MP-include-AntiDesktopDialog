> ⚠ Please note:  
> This README was retroactively created from the
> [original forum post](http://forum.sa-mp.com/showthread.php?t=287941)
> and was modified slightly to remove stuff like weird typos and expired
> links.

# AntiDesktopDialog
> v0.2

Hi guys!  
I present to you the ultimate include to prevent wrong processing of
answered dialogs!

## Storyline:
```
You all know that situation, you're on your desktop and something happens ingame. You get a dialog window, but you can't see it!
So now, you want to go back into the game. How do you do it? You usually press Return or ESC.

Damn!
There's been some important stuff to read and you didn't see it! What can I do to prevent that as the server creator?
```

## The Answer:
**Use this include!**

It prevents incorrect dialog replies from being sent and shows up the
original dialog again!

Simple and clever!  
All you have to do is to include it into your scripts (or a_samp.inc?).
```c
#include <AntiDesktopDialog>
```

**<span style="color: red">Warning</span>**: If you've got a very big
script, it could be that you're using very large dialogs (e.g. a command
for statistics).
So you have to change the array size in line 77!

## Changelog
**[v0.2]**:
* Fixed an issue where dialogs for playerIDs did not hide after a relog/disconnect.

**[v0.1]**:
* First Release

PS: Critics and comments are welcome 
Some testing would be great, too. I tested it, but it still could be buggy because of recent changes.
